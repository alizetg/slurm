=============
Aplicaciones
=============


El software de Magerit se gestiona mediante una herramienta llamada Modules, la cual permite la carga de los módulos adecuados para la ejecución de proyectos siendo posible personalizar de forma dinámica el entorno de cada usuario eligiendo compiladores, librerías, versiones de software, etc.. sugún cada necesidad. En este tutorial vemos los principales comandos de esta aplicación.


Operaciones básicas
-------------------

* **Modules disponibles**


Para ver qué Modules están disponibles para la arquitectura actual se usa el mandato **module avail**.

.. code-block:: bash

    $ module avail


* **Cargar modules**

Para cargar la versión predeterminada de una aplicación disponible se emplea el mandato **module load <app>**.

.. code-block:: bash

	$ module load GCC


Si no queremos la versión predeterminada que se puede identificar porque se encuentra marcada con una (D) al ejecutar el comando module avail,  sino una en concreto debemos complementar el mandato anterior con la versión que se desea cargar **module load <app>/<version>**. Por ejemplo:

.. code-block:: bash

	$ module load  GCC/6.4

El comando anterior se puede sustituir por **module add**


* **Modules cargados**


Podemos consultar los que están actualmente cargados con **module list**.


.. code-block:: bash

	$ module list

* **Cambiar versión de un module previamente cargado**

Para cambiar la versión de una aplicación cargada podemos borrar esa aplicación y posteriormente cargar la versión deseada. Sin embargo, resulta más sencillo y rápido usar el comando **module switch <app><app>/<version a cambiar>**.

Por ejemplo, para cambiar la versión de gcc, compilador de GNU, de la predeterminada a la 7.3, el mandato completo es:

.. code-block:: bash

 	$ module switch GCC GCC/7.3.0-2.30

* **Descargar modules**

El comando para descargar o limpiar un Module del entorno es **module unload <app>**. Si por ejemplo queremos eliminar el module  GCC cargado anteriormente, escribimos:

.. code-block:: bash

	$ module unload GCC


* **Limpiar los modules del entorno**

El comando para limpiar el entorno de los modules cargados es  **module purge**. 

.. code-block:: bash

	$ module purge


Para desarrolladores
--------------------

Los entornos de compilación para desarrolladores con los que cuenta Magerit son:

+-----------------+----------------+-----------------------+----------------+
| Toolchains  	  | Compiladores   | LibrerÍas Matemáticas | Librerías MPI  | 
+=================+================+=======================+================+
| -intel          | Intel          | Intel                 | Intel          |
+-----------------+----------------+-----------------------+----------------+
| -iomkl          | Intel          | Intel                 | OpenMPI        |
+-----------------+----------------+-----------------------+----------------+
| -foss           | GNU            | GNU                   | OpenMPI        |
+-----------------+----------------+-----------------------+----------------+

Actualmente Magerit cuenta con  las siguientes versiones de toolchains 

* **Intel:**

+-----------------+----------------+-----------------------+---------------------+--------------------+
| Versión    	  | GCC            | Compilador Intel      | Versión MPI (impi)  | Versión MKL (imkl) |
+=================+================+=======================+=====================+====================+
| 2016            | 4.9            | 2016.2.181            | 5.1.3.181           | 11.3.2.181         |
+-----------------+----------------+-----------------------+---------------------+--------------------+
| 2017a           | 6.3            | 2017.1.132            | 2017.1.132          | 2017.1.132         |
+-----------------+----------------+-----------------------+---------------------+--------------------+
| 2017b           | 6.4            | 2017.4.196            | 2017.3.196          | 2017.3.196         |
+-----------------+----------------+-----------------------+---------------------+--------------------+
| 2018a           | 6.4            | 2018.1.163            | 2018.1.163          | 2018.1.163         |
+-----------------+----------------+-----------------------+---------------------+--------------------+
| 2018b           | 7.3            | 2018.3.222            | 2018.3.222          | 2018.3.222         |
+-----------------+----------------+-----------------------+---------------------+--------------------+
| 2019a           | 8.2            | 2019.1.144            | 2018.4.274          | 2019.1.144         |
+-----------------+----------------+-----------------------+---------------------+--------------------+


* **Iomkl:**

+-----------------+----------------+-----------------------+--------------------+
| Versión    	  | GCC            | Compilador Intel      | Versión  OpenMPI   | 
+=================+================+=======================+====================+
| 2019.01         | 8.2            | 2019.1.144            | 3.1.3              | 
+-----------------+----------------+-----------------------+--------------------+


* **Foss:**

+-----------------+----------------+-----------------------+---------------------+--------------------+
| Versión    	  | GCC            | Versión  OpenMPI      | Versión OpenBLAS    | FFTW               |
+=================+================+=======================+=====================+====================+
| 2018b           | 7.3            | 3.1.1                 | 0.3.1               |  3.3.8             |
+-----------------+----------------+-----------------------+---------------------+--------------------+
| 2019a           | 8.2            | 3.1.3                 | 0.3.5               |  3.3.8             |
+-----------------+----------------+-----------------------+---------------------+--------------------+


.. topic:: Nota:

   Para obtener más información acerca de las toolchains implementadas en Magerit, puede visitar la web oficial de `Easybuild <https://easybuild.readthedocs.io/en/latest/Common-toolchains.html>`_ .



 



