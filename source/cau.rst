.. _cau:

***********************
Obtener ayuda o soporte
***********************

Para solucionar cualquier duda o problema que surja, el punto de contacto es el `Centro de Atención a Usuarios <mailto:support@cesvima.upm.es>`_ del CeSViMa, dependiente del Vicerrectorado de Servicios Tecnológicos. El servicio se presta únicamente mediante correo electrónico.

En la consulta se debe de indicar siempre que sea posible y de la forma más clara:

* Descripción de la duda o problema surgido.

* Identificación del usuario (`login`) y proyecto asociado, sobre todo si se utiliza una cuenta de correo diferente a la utilizada en el registro.

Si se ha detectado algún problema, se debe añadir:

* Día, fecha y hora aproximados en la que se detectó la incidencia.

* Condiciones en las que se produce.

* Mensajes de error o información mostrados por la máquina en el momento de la incidencia, si se dispone de ellos.

* Si la incidencia es sobre algún trabajo que ejecuta en la máquina, toda la información relativa al trabajo que ocasiona el problema: identificador de trabajo, aplicación ejecutada, mensajes de error, ficheros de log, etc.

Cada una de las comunicaciones iniciadas queda automáticamente registrada en un sistema de control de incidencias asignándoles un identificador único y conservándose su histórico.
