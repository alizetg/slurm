*******************
Descripción general
*******************

La versión actual de Magerit es un clúster de propósito general compuesto por 68 nodos `ThinkSystem SD530 <https://www.lenovo.com/es/es/data-center/servers/high-density/ThinkSystem-SD530/p/77XX7DSSD53>`_, cada uno de ellos, equipados con procesadores `Intel Xeon Gold 6230 <https://ark.intel.com/content/www/us/en/ark/products/192437/intel-xeon-gold-6230-processor-27-5m-cache-2-10-ghz.html>`_ (20 cores @ 2.1 GHz), 192 GB de RAM y un disco SSD de 480 GB.

Esta configuración es capaz de proporcionar una potencia pico de 182.78 TFLOPS.

Nodos
=====

Aunque todos los nodos son idénticos, existen nodos con dos funciones muy diferenciadas:

**Interactivos o de login**
    Son los nodos que permiten el acceso a la infraestructura desde cualquier dispositivo y lugar del mundo. Desde ellos se realiza gestión de trabajos y el intercambio de datos y resultados.

    El acceso se realiza mediante SSH a ``magerit3.cesvima.upm.es`` utilizando las credenciales de usuario que se facilitan con el alta de la cuenta.

    ..  important::

        Las sesiones abiertas se cancelan tras 8 horas de inactividad y, dado que no están pensados para ejecución, se ha establecido un límite de 10 minutos de CPU para cada proceso que ejecute en ellos.

**Cómputo**
    Son los nodos en los que se ejecutan los trabajos y que se encuentran aislados del exterior.

    Las ejecuciones en estos nodos se realizan mediante trabajos por lotes `batch` gestionados por un planificador de recursos.

Los nodos se interconectan con redes 100 GbE de baja latencia.

Almacenamiento
==============

Todos los nodos de Magerit tienen acceso a un espacio de almacenamiento compartido implementado sobre un sistema de ficheros *Open Source* paralelo denominado `Lustre <http://lustre.org>`_.

Cada actividad tiene asignado 1 TB de almacenamiento compartido por todos los miembros y una "carpeta de proyecto" con el formato ``/home/<code>/`` para almacenar la información.

..  note::

    El sistema de ficheros Lustre se encuentra controlado por un sistema de cuotas asignadas a cada grupo, es decir, se considera el total de espacio usado independientemente del miembro que lo utiliza. La coordinación del uso de este espacio de almacenamiento recae sobre el responsable de la actividad.

Bajo esa carpeta aparecen tres tipos de entradas:

**Home de usuario**
    Cada miembro de proyecto tendrá una cuenta de usuario cuya carpeta principal tiene la forma ``/home/<code>/<user>/`` donde puede almacenar su configuración y datos personales.

**Datos compartidos**
    Los datos, resultados o códigos que sean utilizados por varios miembros del proyecto se pueden almacenar en ``/home/<code>/PROJECT/``.

**Datos temporales**
    Para información temporal (logs de ejecuciones, resultados parciales...) existe la ubicación *scratch* en ``/home/<code>/SCRATCH/``.

..  important::

    No se proporciona *backup* garantizado para ninguna de las ubicaciones. Es responsabilidad de cada usuario y responsable del proyecto realizar y gestionar sus propias copias.
