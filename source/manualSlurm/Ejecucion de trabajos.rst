=====================
Ejecución de trabajos
=====================

Magerit se explota mediante trabajos batch usando SLURM como gestor/planificador de recursos. Cuando se envía un trabajo al sistema se indican las características del mismo y SLURM se encargará de reservar los recursos necesarios e iniciar las tareas que se le han indicado.

Definición de un trabajo
------------------------

El trabajo mínimo para ejecutar sería un fichero `job.sh` con el contenido:

::

    #!/bin/bash

    module purge && module load <app>

    srun app --app-param X app_args



El trabajo se envia al sistema ejecutando `sbatch job.sh` y ejecutaría 1 tarea (1 CPU) durante 24 horas. Se puede modificar esta configuración especificando directivas

.. topic:: Nota:

      Aunque las directivas se pueden especificar como argumentos de sbatch en el momento de enviar el trabajo, se recomienda definir las ejecuciones en ficheros para permitir su seguimiento.

::

    #!/bin/bash
    #----------------------- Start job description -----------------------
    #SBATCH --partition=standard
    #SBATCH --job-name=my_job
    #SBATCH --ntasks=1
    #SBATCH --mem-per-cpu=1000
    #SBATCH --time=12:00:00
    #SBATCH --mail-type=ALL
    #SBATCH --mail-user=email@upm.es
    ##------------------------ End job description ------------------------

    module purge && module load <app>

    srun app --app-param X app_args

Las directivas de SLURM que deben ir al principio del jobfile, son comentarios que empiezan con #SBATCH seguido de las opciones de SLURM. Las más comúnmente utilizadas son:

+-------------------------+--------------------+--------------------------------------------------------------------------------------------------+
| Opción completa         | Abreviado          | Descripción                                                                                      |
+=========================+====================+==================================================================================================+
|--job-name=name          |-J name             |Nombre de trabajo (para facilitar el seguimiento) personalizado.                                  |
+-------------------------+--------------------+--------------------------------------------------------------------------------------------------+
|--partition=name         |-p partition        |Partición que se va a utilizar.                                                                   |
+-------------------------+--------------------+--------------------------------------------------------------------------------------------------+
|--nodes=#                |-N #                |Número total de nodos.                                                                            |
+-------------------------+--------------------+--------------------------------------------------------------------------------------------------+
|--ntasks=#               |-n #                |Número de "tareas". Para usar con paralelismo distribuido.                                        |
+-------------------------+--------------------+--------------------------------------------------------------------------------------------------+
|--ntasks-per-node=#      |                    |Número de "tareas" por nodo. Para usar con paralelismo distribuido.                               |
+-------------------------+--------------------+--------------------------------------------------------------------------------------------------+
|--time=[[DD-]HH:]MM:SS   |-t [[DD-]HH:]MM:SS  |Duración máxima de ejecución del trabajo.                                                         |
+-------------------------+--------------------+--------------------------------------------------------------------------------------------------+
|--mem-per-cpu=#          |                    |Memoria solicitada por CPU en MB.                                                                 |
+-------------------------+--------------------+--------------------------------------------------------------------------------------------------+
|--mem=#                  |                    |Memoria solicitada por nodo en MB.                                                                |
+-------------------------+--------------------+--------------------------------------------------------------------------------------------------+
|--mail-user=email        |                    |Dirección de correo (alternativamente, ponga su dirección de correo electrónico en ~ / .forward). |
+-------------------------+--------------------+--------------------------------------------------------------------------------------------------+
|--mail-type=ALL          |                    |Enviar correos electrónicos al usuario en todos los eventos de trabajo.                           |
+-------------------------+--------------------+--------------------------------------------------------------------------------------------------+


.. topic:: Nota:

     * Para obtener más información de las directivas de SLURM ejecute `man sbatch` en la terminal.

     * Tenga en cuenta que Magerit en la ejecución de trabajos  dará más prioridad a los trabajos que menos memoria han consumido, por este motivo recomendamos el uso de mandatos como `--mem-per-cpu` o `--men` en su fichero de ejecución.

Comandos para la ejecución de trabajos
--------------------------------------

Además de la orden sbatch que permite enviar un trabajo al planificador, SLURM proporciona varias formas de controlar un trabajo enviado al sistema.

* `sbatch <https://slurm.schedmd.com/sbatch.html>`_ <job.sh>
  Envía un trabajo al planificador para su posterior ejecución.

* `squeue <https://slurm.schedmd.com/squeue.html>`_
  Muestra el estado de los trabajos del usuario en el sistema pudiendo modificar el formato y la información facilitada.

* `scancel <https://slurm.schedmd.com/scancel.html>`_ <job_id>:
  Cancela un trabajo. Si el trabajo ya estaba ejecutando se abortará la ejecución.

* `sstat <https://slurm.schedmd.com/sstat.html>`_  <job_id>
  Muestra estadísticas de uso de un trabajo que se encuentra en ejecución en el momento

* `sacct <https://slurm.schedmd.com/sacct.html>`_  -j <job_id>:
  Mustra estadísticas de uso de un trabajo ya finalizado (histórico).



Configuración avanzada
----------------------

Magerit permite la ejecución de algunos esquemas avanzados que dependen de la capacidad del software implementado y de lo que se desee hacer. Hay que tener en cuenta en esta configuración que las directivas *ntasks* y *cpus-per-task*, si no se especifican en el jobfile al ejecutar el fichero tomaran por defecto el valor de uno. 


A continuación, se puede observar un ejemplo de los escenarios más comunes:


Trabajos con MPI
****************

MPI (Message Passing Interface) es un mecanismo para la programación paralela por paso de mensajes. Por ello, no es necesario que todos los procesos MPI ejecuten en el mismo nodo, el número de estos procesos se maneja con la directiva `ntasks`.  


::  


    #!/bin/bash
    #----------------------- Start job description -----------------------
    #SBATCH --partition=standard
    #SBATCH --job-name=my_job
    #SBATCH --ntasks=4
    #SBATCH --mem-per-cpu=1000
    #SBATCH --time=00:15:00
    #SBATCH --mail-type=email@upm.es
    ##------------------------ End job description ------------------------


    module purge && module load <app>
    srun app --app-param X app_args



Trabajos secuenciales 
*********************
 
Los trabajos secuenciales suelen soportar OpenMP ya sea directamente en el software o mediante el uso de una librería. Las directivas que se implementan para la ejecución de estos trabajos son `ntasks`y `cpus-per-tasks`.

Ejemplo:


::  


    #!/bin/bash
    #----------------------- Start job description -----------------------
    #SBATCH --partition=standard
    #SBATCH --job-name=my_job
    #SBATCH --ntasks=1
    #SBATCH --cpus-per-task=1
    #SBATCH --mem-per-cpu=1000
    #SBATCH --time=00:15:00
    #SBATCH --mail-type=email@upm.es
    ##------------------------ End job description ------------------------


    module purge && module load <app>
    export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
    srun app --app-param X app_args   


Para acelerar la ejecución de trabajos de este tipo la forma recomendada es:


::  


    #!/bin/bash
    #----------------------- Start job description -----------------------
    #SBATCH --partition=standard
    #SBATCH --job-name=my_job
    #SBATCH --ntasks=1
    #SBATCH --cpus-per-task=40
    #SBATCH --mem-per-cpu=1000
    #SBATCH --time=00:15:00
    #SBATCH --mail-type=email@upm.es
    ##------------------------ End job description ------------------------


    module purge && module load <app>
    export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
    srun app --app-param X app_args



.. topic:: Nota:

      * En los casos anteriores el número de tareas siempre debe ser 1.




Trabajos híbridos
*****************


Los trabajos híbridos (MPI-OpenMP) consisten en ejecutar varios procesos MPI (directiva `ntasks`) cada una de ellos desplegará varios hilos OpenMP (directiva `cpus-per-task`). A efectos contables, equivale a reservar ( `ntasks` x `cpus-per-task` ) CPUS.


::

    
     #!/bin/bash
     #----------------------- Start job description -----------------------
     #SBATCH --ntasks=4
     #SBATCH --cpus_per_task=4
     ##------------------------ End job description ------------------------


     module purge && module load <app> 
     export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
     srun app --app-param X app_args



.. topic:: Nota:

      * Para sacar partido a esta funcionalidad la aplicación debe soportar paralelismo.

 




