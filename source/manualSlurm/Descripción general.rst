===================
Descripción general
===================


El supercomputador Magerit es un clúster de propósito general con arquitectura Intel que permite cubrir las necesidades de cómputo.

Además de la capacidad de almacenamiento local de cada nodo, el sistema dispone de servidores de disco que dan acceso a un disco compartido entre todos los nodos.

La configuración es capaz de proporcionar una potencia pico teórico de  182.784 TFLOPS.

Nodos
-----

Magerit esta compuesto por  68  nodos con 2 procesadores de 20 cores a 2.1GHz (332 GFlops) y con 192 GB de RAM y un disco SSD 480GB.

Las comunicaciones se realizan con dos redes de 25 GbE de baja latencia, una dedicada para el paso de mensajes y la otra dedicada para el resto de las labores.

Aunque todos los nodos comaprten la configuración hardware y software, se dividen en dos funcionalidades básicas:

* **Interactivos o de login**

  Tienen habilitado el acceso al exterior y se utilizan como punto de entrada al sistema. En ellos se realizan labores de edición, compilación, gestión de trabajos e intercambio de ficheros. Para el acceso a Magerit se debe acceder por SSH a `magerit3.cesvima.upm.es`

  Tras esta dirección se encuentran dos máquinas entre las que se repartirá la carga a través de un mecanismo de asignación round-robin.




* **Cómputo**

  Tienen como única misión ejecutar los trabajos de usuario. Estos nodos están completamente aislados del exterior.



Acceso a Magerit
----------------

El acceso al sistema se lleva a cabo gracias a un conjunto de nodos especiales, accesibles desde el exterior por medio del protocolo SSH. Por tanto, es posible entrar en el sistema desde cualquier con un cliente de este tipo (Por ejemplo, el cliente PuTTy).

La dirección de acceso para estos nodos es `magerit3.cesvima.upm.es`. Desde esta, el sistema redirigirá la ejecución a uno de los nodos disponibles de todo el conjunto de la máquina. Las sesiones abiertas se cancelan tras 8h de inactividad, estableciéndose un límite de 10 minutos de ejecución para cada proceso en uno de los nodos interactivos (login), ya que no está permitida la ejecución de software en ellos.

Para entrar al sistema  es necesario contar con un identificador o nombre de usuario, los cuales son facilitados tras el registro. Por otra parte, el método de comunicación con el usuario se realiza mediante la cuenta de correo electrónico especificada al completar el registro. Desde aquí se recibirán notificaciones relativas a la información de la cuenta.



Sistema de ficheros
-------------------

Los nodos de Magerit tienen acceso a un espacio de almacenamiento compartido implementando por un sistema de ficheros distribuido y tolerante a fallos denominado Lustre [#f3]_ .

Cada proyecto existente en Magerit tiene habilitada una zona en el espacio de almacenamiento denominada `/home/<código de proyecto>` la cuál contiene varios tipos de ubicaciones disponibles para los usuarios.

La ubicación principal que se puede encontrar en esta zona es el `home  del usuario`  definida como  `/home/<código_proyecto>/<código_usuario>`  en este espacio cada usuario puede almacenar su configuración del sistema y datos personales.

Otra de las ubicaciones que se puede encontrar es  `/home/<código_proyecto>/PROJECT` donde se proporciona un espacio compartido a todos los miembros del proyecto, por lo que puede utilizarse para almacenar los datos o código que sean usados por múltiples usuarios.

Por último, se puede encontrar la ubicación  `/home/<código_proyecto>/SCRATCH` donde los usuarios puede almacenar los datos temporales por cada proyecto.


.. topic:: Nota:

    * Los archivos con una antigüedad superior a una semana compartidos en `/home/<código_proyecto>/SCRATCH` pueden ser eliminados automáticamente.

    * El sistema de ficheros Lustre se encuentra controlado por un sistema de cuotas asignadas a cada grupo, es decir, se considera el total de espacio usado independientemente del miembro que lo utiliza. La coordinación del uso de este espacio de almacenamiento recae sobre el investigador responsable del proyecto.

    * No se proporciona servicio de backup de ninguno de los sistemas de ficheros, por lo que es responsabilidad de cada usuario y/o investigador principal, realizar y gestionar sus propias copias de seguridad.


.. [#f3]  Lustre. http://lustre.org
