************
Aplicaciones
************

Para simplificar el uso de las aplicaciones instaladas en Magerit se utiliza `Lmod <http://lmod.readthedocs.io>`_, una implementación de `Environment Modules <http://modules.sourceforge.net/>`_. Esta utilidad se encarga de preparar el entorno de ejecución para utilizar distintas versiones de las aplicaciones y sus dependencias mediante la carga de módulos de configuración.

Uso de aplicaciones instaladas
==============================

Las operaciones más comunes cuando se quiere ejecutar aplicaciones ya instaladas:

`avail <https://lmod.readthedocs.io/en/latest/010_user.html#user-s-tour-of-the-module-command>`_
    Muestra todas las aplicaciones disponibles en el sistema.

    ..  code-block:: bash

        module avail

`list <https://lmod.readthedocs.io/en/latest/010_user.html#user-s-tour-of-the-module-command>`_
    Muestra las aplicaciones cargadas actualmente.

    ..  code-block:: bash

        module list

`load <https://lmod.readthedocs.io/en/latest/010_user.html#user-s-tour-of-the-module-command>`_
    Carga la versión predeterminada de una aplicación (aquellas que aparecen marcadas con una ``(D)`` en el listado).

    ..  code-block:: bash

        module load <app>

`load <https://lmod.readthedocs.io/en/latest/010_user.html#user-s-tour-of-the-module-command>`_
    Carga una versión determinada de una aplicación.

    ..  code-block:: bash

        module load <app>/<ver>

`switch <https://lmod.readthedocs.io/en/latest/010_user.html#user-s-tour-of-the-module-command>`_
    Cambia una aplicación por otra.

    ..  code-block:: bash

        module switch <app1>/<ver1> <app2>/<ver2>

    ..  note::

        ``module switch`` es equivalente a ejecutar ``module unload <app1> && module load <app2>/<ver2>``.

`unload <https://lmod.readthedocs.io/en/latest/010_user.html#user-s-tour-of-the-module-command>`_
    Descarga una aplicación.

    ..  code-block:: bash

        module unload <app>

`purge <https://lmod.readthedocs.io/en/latest/010_user.html#user-s-tour-of-the-module-command>`_
    Descarga todas las aplicaciones.

    ..  code-block:: bash

        module purge

..  note::

    Lmod proporciona una utilidad denominada `ml <https://lmod.readthedocs.io/en/latest/010_user.html#ml-a-convenient-tool>`_ que es equivalente a usar ``module`` (todas las operaciones anteriores están soportadas) y añade una sintaxis simplificada para la carga y descarga:

    ``ml <app>``
        Carga una aplicación. Es el equivalente a ``module load <app>``

    ``ml -<app>``
        Descarga una aplicación. Es el equivalente a ``module unload <app>``

    Es posible combinar varios cambios simultáneos ``ml <app1> <app2> -<app3>``

..  note::

    La `Guía del usuario de Lmod <https://lmod.readthedocs.io/en/latest/010_user.html>`_ proporciona más información sobre su uso.

Compilar código propio
======================

Además de las aplicaciones para uso general, también están disponibles compiladores y bibliotecas para compilar código propio. Los conjuntos de estas dependencias básicas son los denominados `toolchains` que se distribuyen también mediante los correspondientes modules.

En Magerit, las compilaciones se realizan con un reducido subconjunto de `toolchains`:

+------------+--------------+-----------------------+--------------+
| Toolchains | Compiladores | Librerías Matemáticas | Librería MPI |
+============+==============+=======================+==============+
| `foss`_    | GNU          | GNU                   | OpenMPI      |
+------------+--------------+-----------------------+--------------+
| `intel`_   | Intel        | Intel                 | Intel        |
+------------+--------------+-----------------------+--------------+
| iomkl      | Intel        | Intel                 | OpenMPI      |
+------------+--------------+-----------------------+--------------+

.. _intel: https://easybuild.readthedocs.io/en/latest/Common-toolchains.html#intel-toolchain
.. _foss: https://easybuild.readthedocs.io/en/latest/Common-toolchains.html#foss-toolchain

..  note::

    Las versiones específicas que componen cada `toolchain` están descritas en los `Common toolchains de Easybuild <https://easybuild.readthedocs.io/en/latest/Common-toolchains.html#overview-of-common-toolchains>`_.

    El `toolchain` `iomkl` es idéntico al `intel`_ reemplazando la librería MPI de Intel por la versión OpenMPI incorporada en `foss`_. Esta configuración permite utilizar características únicas de OpenMPI mientras se usan las bibliotecas optimizadas de Intel.

Para configurar el entorno para utilizar cualquiera de los `toolchains` es necesario cargar el modulo correspondiente como para cualquier otro tipo de software.

..  code-block:: bash

    module load <intel|iomkl|foss>

A partir de este momento, estará cargado todo el entorno de compilación: compiladores, librerias matematicas, librerias de paso de mensajes...
