.. Slurm documentation master file, created by
   sphinx-quickstart on Thu Feb  1 12:12:04 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Guía del usuario HPC
====================

A finales de 2004, la Universidad Politécnica de Madrid decide acometer la falta de recursos de cómputo disponibles para investigación. El resultado de esta iniciativa es una infraestructura de cómputo denominada Magerit, gestionada desde el CeSViMa, dependiente del Vicerrectorado de Servicios Tecnológicos.

Magerit es un sistema de cómputo de propósito general en constante evolución centrado en cubrir las necesidades de la UPM. En su segunda versión se convirtió en el más potente de España en la lista TOP500 de junio de 2011. También se posicionó como uno de los supercomputadores energéticamente más eficientes del mundo.

La versión actual de Magerit, mantiene estos objetivos y se centra en facilitar al máximo el acceso y uso de los recursos permitiendo ejecutar prácticamente cualquier trabajo que precise una investigación.

..  important::

    Esta documentación está en proceso de mejora contínua por lo que su contenido se modificará según se realicen actualizaciones.

    Los cambios importantes serán notificados a través de las listas de correo.

.. toctree::
    :caption: Contenidos
    :name: toc
    :maxdepth: 3
    :numbered:
    :glob:

    /descripcion-general
    /aplicaciones
    /ejecucion-trabajos
    /cau
    /faq

..  note::

    Para solucionar cualquier duda o problema que surja, el punto de contacto es el :ref:`Centro de Atención a Usuarios <cau>` del CeSViMa.
