**************************
Preguntas frecuentes (FAQ)
**************************

Esta sección está en constante actualización y algunas preguntas pueden contener información obsoleta o inválida.

Gestión de trabajos
===================

**He enviado un trabajo al sistema ¿Cómo puedo saber el estado en el que se encuentra?**
    Cada usuario puede ver sus trabajos enviados al sistema con `squeue <https://slurm.schedmd.com/squeue.html>`_. Es posible generar un informe más detallado junto al desempeño con `sstat <https://slurm.schedmd.com/sstat.html>`_ (si está ejecutando) o con `sacct <https://slurm.schedmd.com/sacct.html>`_ (si ya ha finalizado la ejecución).

**¿Puedo hacer que mi trabajo esté menos tiempo esperando?**
    Una forma de reducir el tiempo de espera es ajustando al máximo los recursos solicitados, concretamente, reduciendo al máximo el número de tareas (directivas ``--ntasks`` y ``--cpus-per-task``) así como la duración del trabajo (directiva ``--time``). De esta forma será más fácil que el planificador pueda poner en ejecución un trabajo que precisa una ventana de tiempo más pequeña.

    Otra forma de reducir el tiempo de ejecución de un trabajo de desarrollo o de alta prioridad con objetivo de depurarlo es indicar la partition `debug` en el fichero de ejecución.

**¿Cuál es la mejor forma de ejecutar varios trabajos que tienen dependencias entre ellos?**
    Para definir las dependencias se utiliza la directiva ``--dependency`` indicando los identificadores de trabajo de los que depende y cómo deben haber acabado. Hay más información en el `manual de sbatch <https://slurm.schedmd.com/sbatch.html>`_ on ejecutando ``man sbatch`` en un nodo.

Aplicaciones
============

**Necesito una aplicación que no aparece instalada en el sistema**
    Las aplicaciones de terceros, para evitar que existan múltiples copias de la misma aplicación en el sistema, son instaladas por el equipo de administración. Solicite su instalación a través del :ref:`Centro de Atención a Usuarios <cau>` del CeSViMa.

    Las aplicaciones que son instaladas por el usuario son los desarrollos propios del usuario o aquellas que por su diseño no permitan el uso compartido por múltiples usuarios.

**Necesito una aplicación que precisa licencia**
    El software con licencia debe ser siempre controlado por el equipo de administración y el acceso al mismo estará permitido únicamente a aquellos proyectos/usuarios que hayan acreditado disponer de una licencia válida.

    Para que se habilite el acceso al software es necesario enviar una copia de la misma al equipo de administración. Para ello basta con ponerse en contacto con el :ref:`Centro de Atención a Usuarios <cau>` del CeSViMa.


Errores típicos
===============

``bad interpreter: No such file or directory``
    La codificación del retorno de carro es incorrecta, posiblemente usa la codificación de Windows, y el sistema no es capaz de interpretarlo.

    En este caso basta con ejecutar ``dos2unix`` sobre fichero del job para subsanar el problema.

``ssh_exchange_identification: Connection closed by remote host``
    Debido a los ataques recibidos en el sistema, existe un mecanismo de bloqueo automático de las IPs que intentan realizar accesos fraudulentos. Cuando una IP queda bloqueada se recibe ese mensaje en la conexión.

    El bloqueo se produce cuando se realizan múltiples intentos de acceso (más de tres) con credenciales incorrectas (nombre de usuario inválido o contraseña incorrecta). Al detectar cualquiera de estas condiciones se bloquea el acceso a cualquier servicio desde esa IP en todos los nodos de login del sistema.

    Para liberar el bloqueo, debe enviarse la dirección IP desde la que se realiza la conexión al :ref:`Centro de Atención a Usuarios <cau>`.

Miscelánea
==========

**¿Cómo puedo contactar con el CeSViMa?**
    La principal fuente de información es la página web del `CeSViMa <https://www.cesvima.upm.es>`_ o ponerse en contacto con :ref:`Centro de Atención a Usuarios <cau>` del CeSViMa.



